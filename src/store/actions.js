import { firestore } from '@/plugins/firebase'

var actions = {

  // 初期値の取得
  async init ({ commit, state, dispatch }, categoryId) {
    let groupId = state.groupId
    let docRef = firestore.collection('groups').doc(groupId)

    // ファミリーネームの取得
    docRef.get()
      .then((doc) => {
        if (doc.exists) {
          let data = doc.data()
          commit('SET_FAMILYNAME', data.name)
          commit('SET_CATEGORIES', data.categories)
        } else {
          console.log('No such document!')
        }
      }).catch(function (error) {
        console.log('Error getting document:', error)
      })

    // カテゴリーの取得
    await dispatch('getCategories')
    await dispatch('setCategory', categoryId)
  },

  // カテゴリーの取得
  async getCategories ({ commit, state }) {
    let array = []
    let groupId = state.groupId
    let docRef = firestore.collection('groups').doc(groupId)
    await docRef.collection('categories')
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          array.push({
            id: doc.id,
            name: doc.data().name
          })
        })
        commit('SET_CATEGORIES', array)
      })
      .catch(function (error) {
        console.log('Error getting documents: ', error)
      })
  },

  // カテゴリーの選択とデータの取得
  async setCategory ({ commit, state }, categoryId) {
    // カテゴリーの設定
    let categories = state.categories
    let filtered = categories.filter((category) => {
      return category.id === categoryId
    })
    if (filtered.length === 1) {
      commit('SET_CURRENT_CATEGORY', filtered[0])
    } else {
      commit('SET_CURRENT_CATEGORY', categories[0])
    }

    // 支払いデータの取得
    let array = []
    let groupId = state.groupId
    let docRef = firestore.collection('groups').doc(groupId)
    await docRef.collection('payments').where('category', '==', categoryId)
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          array.push(doc.data())
        })
        commit('SET_ITEMS', array)
      })
      .catch(function (error) {
        console.log('Error getting documents: ', error)
      })
  },
  // 支払いデータの追加
  postData ({ commit, state, dispatch }, data) {
    let groupId = state.groupId
    let docRef = firestore.collection('groups').doc(groupId)

    let doc = {
      when: new Date(data.when),
      cost: Number(data.cost),
      category: state.currentCategory.id,
      who: data.who,
      what: data.what
    }
    docRef.collection('payments').add(doc)
      .then((docRef) => {
        console.log(doc)
        console.log('Document written with ID:', docRef.id)
        dispatch('setCategory', state.currentCategory.id)
      })
      .catch((error) => {
        console.error('Error adding document: ', error)
      })
  }
}

export default actions
