import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    groupId: '6dEXfrmsLeGu5ZYhGCUT',
    familyName: '',
    currentCategory: {
      id: '',
      name: ''
    },
    categories: [],
    items: []
  },
  mutations: {
    SET_FAMILYNAME (state, name) {
      state.familyName = name
    },
    SET_CATEGORIES (state, categories) {
      state.categories = categories
    },
    SET_CURRENT_CATEGORY (state, category) {
      state.currentCategory = {
        id: category.id,
        name: category.name
      }
    },
    SET_ITEMS (state, items) {
      state.items = items
    }
  },
  actions: actions
})
