import firebase from 'firebase'
import 'firebase/firestore'
import config from '../../config/firebase'

firebase.initializeApp(config)
export const firestore = firebase.firestore()
